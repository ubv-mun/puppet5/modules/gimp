# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include gimp::install
class gimp::install {
  package { $gimp::package_name:
    ensure => $gimp::package_ensure,
  }
}
